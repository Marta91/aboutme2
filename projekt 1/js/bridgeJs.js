tl = new TimelineMax({delay:1, repeat:-1, repeatDelay:2});

var road1 = MorphSVGPlugin.pathDataToBezier("#chemin1",{align:"relative",offsetX:0,offsetY:-5});
var road2 = MorphSVGPlugin.pathDataToBezier("#chemin2",{align:"relative"});
var road3 = MorphSVGPlugin.pathDataToBezier("#chemin3",{align:"relative"});

tl
  .staggerFromTo($("#guineapigs > g:not(#yopick)"),0.15,{rotation:-5},{rotation:5,yoyo:true,repeat:-1},0.4,0)
  .fromTo($("#guineapigs .cls-32"),0.15,{transformOrigin: "top right",rotation:-25},{rotation:25,yoyo:true,repeat:-1},0)
  .fromTo($("#yopick"),0.15,{rotation:-25},{rotation:25,yoyo:true,repeat:-1},0)
  .staggerTo($("#guineapigs > g:not(#yopick)"),5,{bezier:{values:road1, type:"cubic",autoRotate: false},transformOrigin: "50% 100%",ease:Linear.easeNone, repeat:-1},0.6,0)
.to($("#yopick"),3,{bezier:{values:road1, type:"cubic",autoRotate: false},transformOrigin: "50% 100%",ease:Linear.easeNone, repeat:-1,delay:3,repeatDelay:3},0.6,0)
.fromTo($('#dancing'),1,{transformOrigin:"center center",rotation:-5},{rotation:5,repeat:-1,yoyo:true,ease:Circ.easeInOut},0)
.fromTo($('#dancing .cls-32'),1,{transformOrigin:"top center",rotation:-5},{rotation:25,repeat:-1,yoyo:true,ease:Circ.easeInOut},0)
;
