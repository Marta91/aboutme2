function hideAll (){
	$('#homePage'). hide();
	$('#skillsPage'). hide();
	$('#coursesPage'). hide();
	$('#contactPage'). hide();
	$('#bridgePage'). hide();
	$('.active'). removeClass('active');
}

$(document).ready(function() {
	console.log('ready!');
	
	$('#home').on('click', function () {
		hideAll();
		$('#homePage').addClass('active');
		$('#homePage').show('slow');
	})
	
	$('#skills').on('click', function () {
		hideAll();
		$('#skillsPage').addClass('active');
		$('#skillsPage').show('slow');
	})
	
	$('#courses').on('click', function () {
		hideAll();
		$('#coursesPage').addClass('active');
		$('#coursesPage').show('slow');
	})
	
	$('#contact').on('click', function () {
		hideAll();
		$('#contactPage').addClass('active');
		$('#contactPage').show('slow');
	})
	
	$('#bridge').on('click', function () {
		hideAll();
		$('#bridgePage').addClass('active');
		$('#bridgePage').show('slow');
	})
	
	$('#contactPageForm').validate({
        
        rules : {
            "name" : {required : true, minlength: 3},
            "email" : {required : true, email : true},
            "content" : { required : true, minlength: 10}
        },
        
        messages : {
            "name" : "Przedstaw się.",
            "email" : "Wprowadź poprawny email",
            "content" : "Przydałaby się jakaś treść"
        }
        
    });
	
	
});


